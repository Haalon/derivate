{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE IncoherentInstances #-}

module Chain where

data ChainBase a b = Head b | Tail (a -> ChainBase a b)
type Chain a = ChainBase a a

instance Functor (ChainBase a) where
    -- (a -> b) -> Chain a -> Chain b
    fmap f (Head val) = Head $ f val
    fmap f (Tail fun) = Tail (\v -> fmap f (fun v))

instance Applicative (ChainBase a) where
    pure = Head
    -- ChainBase a (b -> c) -> ChainBase a b -> ChainBase a c
    (Head fun) <*> (Head val) = Head $ fun val
    (Tail fun_to_ab) <*> cval = Tail $ \val_a -> fun_to_ab val_a <*> cval
    cfun <*> (Tail fun_to_b) = Tail $ \val_a -> cfun <*> fun_to_b val_a    

instance Monad (ChainBase a) where
    return = pure
    --  ChainBase a b -> (b -> ChainBase a c) -> ChainBase a c
    (Head val) >>= bfun = bfun val
    (Tail fun) >>= bfun = Tail $ \val_a -> fun val_a >>= bfun

class Chainable a b r where 
    chain :: r -> ChainBase a b
    unChain :: ChainBase a b -> r

instance Chainable a b b where
    chain = Head
    unChain (Head val) = val    

instance Chainable a b r => Chainable a b (a -> r) where
    chain f = Tail (\v -> chain $ f v)
    unChain (Tail fun) = \val -> unChain $ fun val

feed :: ChainBase a b -> a -> ChainBase a b
feed (Tail f) val = f val
feed chain _ = chain

feedList :: ChainBase a b -> [a] -> ChainBase a b
feedList = foldl feed

free :: ChainBase a b -> Maybe b
free (Head val) = Just val
free _ = Nothing

-- unWrap :: ChainBase a b -> ChainBase a (a->b)
-- unWrap (Head val) = Head $ const val
-- unWrap other = switch other
--   where
--      switch (Head val) = val
--      switch (Tail fun) = (\val -> switch $ fun val)


-- -- applyList :: Chainable a r => r -> [a] -> Maybe a
-- -- applyList f ls = unChain $ foldl feed (chain f) ls